package org.chainmaker.contracts.docker.java.sandbox;

import com.google.protobuf.ByteString;
import org.chainmaker.contracts.docker.java.pb.proto.DockerVMCode;
import org.chainmaker.contracts.docker.java.pb.proto.DockerVMMessage;
import org.chainmaker.contracts.docker.java.pb.proto.DockerVMType;
import org.chainmaker.contracts.docker.java.pb.proto.SysCallMessage;
import org.chainmaker.contracts.docker.java.sandbox.utils.TxDuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class RuntimeClient extends GrpcStreamClient {

    private final Logger logger = LoggerFactory.getLogger(RuntimeClient.class.getName());

    public RuntimeClient(String host, int port) {
        super(host, port);
    }

    public DockerVMMessage unarySendMsg(DockerVMMessage requestMsg) throws InterruptedException {

        TxDuration.currentTxDuration.startSysCall(requestMsg);

        DockerVMMessage responseMsg = super.unarySendMsg(requestMsg);

        TxDuration.currentTxDuration.endSysCall(requestMsg);

        return responseMsg;

    }

}
