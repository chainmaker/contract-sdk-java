package org.chainmaker.contracts.docker.java.sandbox;

import ch.qos.logback.classic.Level;
import com.google.protobuf.ByteString;
import org.chainmaker.contracts.docker.java.pb.proto.*;
import org.chainmaker.contracts.docker.java.sandbox.annotaion.ContractMethod;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.chainmaker.contracts.docker.java.sandbox.utils.StepStatistics;
import org.chainmaker.contracts.docker.java.sandbox.utils.TxDuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import sun.misc.Signal;
import sun.misc.SignalHandler;

import static org.chainmaker.contracts.docker.java.sandbox.utils.TxDuration.currentStatus;
import static org.chainmaker.contracts.docker.java.sandbox.utils.TxDuration.currentTxDuration;

public class Sandbox {

    private static RuntimeClient runtimeClient;

    private static EngineClient engineClient;

    private static Logger logger;

    private static IContract contract;

    private static final Map<String, Method> methodMap = new HashMap<>();

    public static void serve(String[] args, IContract contractClazz){
        // load config
        ConfigCtx.loadConfig(args);

        currentStatus = TxDuration.TxStatus.BEFORE_RECEIVE;

        logger = LoggerFactory.getLogger(ConfigCtx.processName);

        SignalHandler sigtermHandler = signal -> {
            logger.info("{} SignalHandler: {}, exit normally", ConfigCtx.processName, signal);
            logger.debug("sandbox - end");
            System.exit(signal.getNumber());
        };

        SignalHandler sigintHandler = signal -> {
            logger.info("{} SignalHandler: {}", ConfigCtx.processName, signal);
            // print tx duration and stack
            logger.info("syscall statistics of current tx {}, start time: {}, end time: {}, current status: {}, syscalls: {}, steps: {}",
                    currentTxDuration.getTxId(), currentTxDuration.getStartTime(), currentTxDuration.getEndTime(),
                    currentStatus, currentTxDuration.printSysCallList(), StepStatistics.printTxSteps(currentTxDuration.getMsg()));
            // logger.info("stack info: {}", Thread.currentThread().getStackTrace());

            logger.debug("sandbox - end");
            System.exit(signal.getNumber());
        };

        Signal.handle(new Signal("INT"), sigintHandler); // kill -2 PID
        Signal.handle(new Signal("TERM"), sigtermHandler); // kill -15 PID


        // init service client
        runtimeClient = new RuntimeClient(ConfigCtx.runtimeServiceHost, ConfigCtx.runtimeServicePort);
        engineClient = new EngineClient("127.0.0.1", ConfigCtx.engineServicePort);

        SDK.setRuntimeClient(runtimeClient);
        // assume that the logger is of type  ch.qos.logback.classic.Logger so that we can
        // set its level
        ch.qos.logback.classic.Logger rootLogger =
                (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
        //set its Level to INFO. The setLevel() method requires a logback logger
        rootLogger.setLevel(Level.toLevel(ConfigCtx.logLevel, Level.INFO));

        SDK.setLogger(LoggerFactory.getLogger(contractClazz.getClass()));

        // 查找合约类和合约方法
        contract = contractClazz;
        lookupContractMethod(contractClazz.getClass());

        // 注册合约进程，准备处理交易
        engineClient.registerSandbox();


        // waiting for txrequest
        while (true) {
            try {
                DockerVMMessage txRequest = engineClient.blockingGetMsg();
                String method = txRequest.getRequest().getMethod();
                String chainId = txRequest.getChainId();
                String contractName = txRequest.getRequest().getContractName();
                String txId = txRequest.getTxId();
                Map<String, ByteString> parameters = txRequest.getRequest().getParametersMap();

                currentTxDuration.reset(txRequest);
                currentStatus = TxDuration.TxStatus.BEFORE_EXECUTE;

                CrossContext crossContext = txRequest.getCrossContext().toBuilder()
                        .setProcessName(ConfigCtx.processName).build();

                Context context = new Context(chainId, contractName, txId, method, parameters, crossContext);
                SDK.setContext(context);

                Response response;
                TxResponse txResponse;

                try {
                    Method method1 = methodMap.get(method);
                    if (method1 == null) {
                        txResponse = buildFailResponse("invoke method not found", context);
                    } else {
                        logger.debug("class instance: " + contract.toString());
                        logger.debug("params " + parameters.toString());
                        logger.debug("method " + method1.toString());
                        currentStatus = TxDuration.TxStatus.EXECUTING;

                        response = (Response) method1.invoke(contract);

                        logger.debug(response.toString());

                        // 返回给runtime 合约的执行结果
                        txResponse = buildTxResponse(response, context);
                    }
                } catch ( Exception e ){
                    e.printStackTrace(System.out);
                    logger.warn("invoking contract error, {}", e.getCause().toString());
                    txResponse = buildFailResponse("contract runtime exception", context);
                }
                currentStatus = TxDuration.TxStatus.AFTER_EXECUTED;

                DockerVMMessage newMsg = StepStatistics.enterNextStep(txRequest, StepType.SANDBOX_SEND_CHAIN_RESP, "");

                DockerVMMessage responseMsg = DockerVMMessage.newBuilder()
                        .setChainId(chainId).setTxId(txId)
                        .setType(DockerVMType.TX_RESPONSE)
                        .setResponse(txResponse)
                        .addAllStepDurations(newMsg.getStepDurationsList())
                        .build();

                runtimeClient.sendMsg(responseMsg);

                currentStatus = TxDuration.TxStatus.AFTER_SEND_RESPONSE;


                //返回给engine 完成消息，触发下一个执行任务
                DockerVMMessage.Builder txCompletedMsgBuilder = DockerVMMessage.newBuilder()
                        .setChainId(chainId)
                        .setTxId(txId)
                        .setCrossContext(crossContext)
                        .addAllStepDurations(newMsg.getStepDurationsList());

                if (txResponse.getCode().equals(DockerVMCode.FAIL) && (method.equals("init_contract") || method.equals("upgrade"))) {
                    txCompletedMsgBuilder.setType(DockerVMType.ERROR);
                } else {
                    txCompletedMsgBuilder.setType(DockerVMType.COMPLETED);
                }

                engineClient.sendMsg(txCompletedMsgBuilder.build());

                currentStatus = TxDuration.TxStatus.FINISHED;
                currentTxDuration.end();
                logger.debug(currentTxDuration.toString());

            } catch (Exception e) {
                logger.debug("sandbox - end");
                logger.debug(currentTxDuration.toString());
                throw new RuntimeException(e);
            }

        }

    }

    public static TxResponse buildTxResponse(Response response, Context context) {

        TxResponse.Builder txResponseBuilder = TxResponse.newBuilder().setChainId(context.getChainId()).setTxId(context.getTxId());

        if (response.getStatus() == DockerVMCode.OK_VALUE) {
            return buildSuccessResponse(response.getPayload(), context);

        } else {
            txResponseBuilder.setCode(DockerVMCode.FAIL);
            return buildFailResponse(response.getMessage(), context);
        }


    }

    private static TxResponse buildSuccessResponse(ByteString payload, Context context){
        TxResponse.Builder txResponseBuilder = TxResponse.newBuilder().setChainId(context.getChainId()).setTxId(context.getTxId());

        txResponseBuilder.setCode(DockerVMCode.OK);
        txResponseBuilder.setResult(payload);
        txResponseBuilder.putAllWriteMap(context.getWriteSet());
        txResponseBuilder.putAllReadMap(context.getReadSet());
        txResponseBuilder.addAllEvents(context.getEvents());

        return txResponseBuilder.build();
    }

    private static TxResponse buildFailResponse(String msg, Context context){
        TxResponse.Builder txResponseBuilder = TxResponse.newBuilder().setChainId(context.getChainId()).setTxId(context.getTxId());

        txResponseBuilder.setCode(DockerVMCode.FAIL);
        txResponseBuilder.setMessage(msg);
        txResponseBuilder.putAllWriteMap(context.getWriteSet());
        txResponseBuilder.putAllReadMap(context.getReadSet());
        txResponseBuilder.addAllEvents(context.getEvents());

        return txResponseBuilder.build();
    }

    public static void lookupContractMethod(Class<?> clazz) {
        for (final Method m : clazz.getMethods()) {
            if (m.getAnnotation(ContractMethod.class) != null) {
                logger.debug("Found annotated method " + m.getName());

                methodMap.put(m.getName(), m);

            }
        }
        try {
            methodMap.put("init_contract", clazz.getMethod("initContract"));
            methodMap.put("upgrade", clazz.getMethod("upgradeContract"));
        } catch (NoSuchMethodException e){
            logger.error("must have methods initContract and upgradeContract");
            System.exit(-1);
        }

    }


}
