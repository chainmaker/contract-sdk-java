package org.chainmaker.contracts.docker.java.sandbox;

import com.google.protobuf.ByteString;
import org.chainmaker.contracts.docker.java.pb.proto.CrossContext;
import org.chainmaker.contracts.docker.java.pb.proto.Event;
import org.chainmaker.contracts.docker.java.sandbox.utils.ChainUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;

public class Context {

    private String chainId;

    private String txId;

    private String contractName;

    private String method;

    private String originalTxId;

    private final Map<String, ByteString> args;

    private Map<String, ByteString> readSet;
    private Map<String, ByteString> writeSet;


    private String creatorOrgId;

    private String creatorRole;

    private String creatorPk;

    private String senderOrgId;

    private String senderRole;

    private String senderPk;

    private String senderAddress;

    private String blockHeight;

    private String txTimestamp;

    private List<Event> events;

    private CrossContext crossCtx;

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    private String origin;


    public Context(String chainId, String contractName, String txId, String method, Map<String, ByteString> parameters, CrossContext crossContext) {
        this.chainId = chainId;
        this.contractName = contractName;
        this.txId = txId;
        this.method = method;
        this.originalTxId = txId.split("#")[0];

        this.creatorOrgId = ChainUtils.byteStringToUTF8String(parameters.get(ChainUtils.ContractCreatorOrgIdParam));
        this.creatorRole = ChainUtils.byteStringToUTF8String(parameters.get(ChainUtils.ContractCreatorRoleParam));
        this.creatorPk = ChainUtils.byteStringToUTF8String(parameters.get(ChainUtils.ContractCreatorPkParam));
        this.senderOrgId = ChainUtils.byteStringToUTF8String(parameters.get(ChainUtils.ContractSenderOrgIdParam));
        this.senderRole = ChainUtils.byteStringToUTF8String(parameters.get(ChainUtils.ContractSenderRoleParam));
        this.senderPk = ChainUtils.byteStringToUTF8String(parameters.get(ChainUtils.ContractSenderPkParam));
        this.senderAddress = ChainUtils.byteStringToUTF8String(parameters.get(ChainUtils.ContractCrossCallerParam));
        this.blockHeight = ChainUtils.byteStringToUTF8String(parameters.get(ChainUtils.ContractBlockHeightParam));
        this.txTimestamp = ChainUtils.byteStringToUTF8String(parameters.get(ChainUtils.ContractTxTimestamp));

        this.readSet = new HashMap<>();
        this.writeSet = new HashMap<>();
        this.events = new ArrayList<Event>();

        this.args = new HashMap<>();
        for (Map.Entry<String, ByteString> entry : parameters.entrySet()) {
            if (!ChainUtils.SysParamKeys.contains(entry.getKey())){
                this.args.put(entry.getKey(), entry.getValue());
            }
        }

        this.crossCtx = crossContext;

    }

    public Map<String, ByteString> getReadSet() {
        return readSet;
    }

    public Map<String, ByteString> getWriteSet() {
        return writeSet;
    }

    public void resetWriteSet() {
        writeSet = new HashMap<>();
    }

    public void addReadSet(String key, ByteString value) {
        readSet.put(key, value);
    }

    public void addReadSet(String key, byte[] value) {
        addReadSet(key, ByteString.copyFrom(value));
    }

    public void addReadSet(String key, String value) {
        addReadSet(key, ByteString.copyFromUtf8(value));
    }

    public void addWriteSet(String key, ByteString value) {
        writeSet.put(key, value);
    }

    public void addWriteSet(String key, byte[] value) {
        addWriteSet(key, ByteString.copyFrom(value));
    }

    public void addWriteSet(String key, String value) {
        addWriteSet(key, ByteString.copyFromUtf8(value));
    }

    public String getChainId() {
        return chainId;
    }

    public String getTxId() {
        return txId;
    }

    public String getOriginalTxId() {
        return originalTxId;
    }
    public String getContractName() {
        return contractName;
    }

    public String getMethod() {
        return method;
    }

    public byte[] getParamBytes(String key) {
        ByteString value = args.get(key);
        return value == null? null : value.toByteArray();
    }

    public String getParamString( String key){
        ByteString value = args.get(key);
        return value == null ? null : value.toStringUtf8();
    }

    public List<Event> getEvents() { return events; }

    public void addEvent(Event e) {
        events.add(e);
    }

    public Map<String, ByteString> getArgs() { return args; }


    public String getCreatorOrgId() { return creatorOrgId; }

    public String getCreatorRole() { return creatorRole; }

    public String getCreatorPk() { return creatorPk; }

    public String getSenderOrgId() { return senderOrgId; }

    public String getSenderRole() { return senderRole; }

    public String getSenderPk() { return senderPk; }

    public String getSenderAddress() { return senderAddress; }

    public String getBlockHeight() { return blockHeight; }

    public String getTxTimestamp() { return txTimestamp; }

    public CrossContext getCrossCtx() { return crossCtx; }

    public void setCrossCtx( CrossContext crossContext) { this.crossCtx = crossContext; }

    public String concatKey(String key, String field) throws IOException {
        ByteArrayOutputStream concatKey = new ByteArrayOutputStream( );
        concatKey.write( contractName.getBytes() );
        concatKey.write( new byte[]{'#'} );
        concatKey.write( key.getBytes() );
        if (field.length() > 0) {
            concatKey.write( new byte[]{'#'} );
            concatKey.write( field.getBytes() );
        }
        return concatKey.toString();
    }

}


