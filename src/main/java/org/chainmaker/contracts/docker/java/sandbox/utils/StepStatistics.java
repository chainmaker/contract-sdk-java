package org.chainmaker.contracts.docker.java.sandbox.utils;

import org.chainmaker.contracts.docker.java.pb.proto.*;
import org.chainmaker.contracts.docker.java.sandbox.ConfigCtx;

import java.time.Instant;
import java.util.List;

public class StepStatistics {

    public static DockerVMMessage enterNextStep(DockerVMMessage msg, StepType stepType, String log) {
        if (ConfigCtx.disableSlowLog) {
            return msg;
        }

        DockerVMMessage.Builder msgBuilder = msg.toBuilder();

        List<StepDuration.Builder> stepDurationList = msgBuilder.getStepDurationsBuilderList();

        if (!stepDurationList.isEmpty()) {
            int stepLen = stepDurationList.size();
            StepDuration.Builder currStep = stepDurationList.get(stepLen - 1);
            StepDuration.Builder firstStep = stepDurationList.get(0);
            long curTime = Instant.now().getEpochSecond() * 1_000_000_000 + Instant.now().getNano();
            currStep.setUntilDuration(curTime - firstStep.getStartTime());
            currStep.setStepDuration(curTime - currStep.getStartTime());
            // msgBuilder.setStepDurations(stepLen - 1, currStep);
        }

        StepDuration.Builder stepDur = StepDuration.newBuilder()
                .setType(stepType)
                .setMsg(log)
                .setStartTime(Instant.now().getEpochSecond() * 1_000_000_000 + Instant.now().getNano());

        msgBuilder.addStepDurations(stepDur);

        return msgBuilder.build();

    }


    public static String printTxSteps(DockerVMMessage msg) {
        StringBuilder sb = new StringBuilder();
        for (StepDuration step : msg.getStepDurationsList()) {
            sb.append(String.format("<step: %s, start time: %s, step cost: %dms, until cost: %dms, msg: %s> ",
                    step.getType(), step.getStartTime(),
                    step.getStepDuration(),
                    step.getUntilDuration(),
                    step.getMsg()));
        }
        return sb.toString();
    }

}
