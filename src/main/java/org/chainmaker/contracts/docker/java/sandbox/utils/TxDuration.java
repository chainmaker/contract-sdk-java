package org.chainmaker.contracts.docker.java.sandbox.utils;


import org.chainmaker.contracts.docker.java.pb.proto.DockerVMMessage;
import org.chainmaker.contracts.docker.java.pb.proto.DockerVMType;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;


public class TxDuration {

    public static TxDuration	currentTxDuration = new TxDuration();
    public static TxStatus currentStatus = TxStatus.BEFORE_LISTENING;

    public enum TxStatus {
        BEFORE_LISTENING,
        BEFORE_RECEIVE,
        BEFORE_EXECUTE,
        EXECUTING,
        WAITING_SYS_CALL_RESPONSE,
        AFTER_EXECUTED,
        AFTER_SEND_RESPONSE,
        FINISHED
    }


    private long startTime;
    private long endTime;
    private final List<SysCallRecord> sysCalls;

    private DockerVMMessage msg;

    private int sysCallCnt;
    private long sysCallDuration;
    private int crossCallCnt;
    private long crossCallDuration;

    public TxDuration() {
        this.sysCalls = new ArrayList<>();
    }

    public TxDuration( DockerVMMessage msg) {
        this.msg = msg;
        this.startTime = Instant.now().getEpochSecond() * 1_000_000_000 + Instant.now().getNano();
        this.sysCalls = new ArrayList<>();
    }

    public long getStartTime() {
        return startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public String getTxId() {
        return msg.getTxId();
    }

    public DockerVMMessage getMsg() {
        return msg;
    }

    public void start() {
        this.startTime = Instant.now().getEpochSecond() * 1_000_000_000 + Instant.now().getNano();

    }

    public void end() {
        this.endTime = Instant.now().getEpochSecond() * 1_000_000_000 + Instant.now().getNano();

    }

    public long getDuration() {
        return this.endTime - this.startTime;
    }



    public void reset(DockerVMMessage msg) {
        this.msg = msg;
        this.startTime = Instant.now().getEpochSecond() * 1_000_000_000 + Instant.now().getNano();
        this.endTime = this.startTime;
        this.sysCallCnt = 0;
        this.sysCallDuration = 0;
        this.crossCallCnt = 0;
        this.crossCallDuration = 0;
        this.sysCalls.clear();
    }


    public String toString() {
        return String.format("[%s] start: %d, spend: %dμs, syscall: %dμs(%d), cross contract: %dμs(%d)",
                msg.getTxId(), startTime, getDuration() / 1_000,
                sysCallDuration / 1_000, sysCallCnt,
                crossCallDuration / 1_000, crossCallCnt);
    }

    public String printSysCallList() {
        StringBuilder sb = new StringBuilder();
        sb.append(msg.getTxId());
        sb.append(": ");
        if (sysCalls.isEmpty()) {
            sb.append("no syscalls");
        } else {
            for (SysCallRecord sysCallTime : sysCalls) {
                sb.append(sysCallTime.toString());
            }
        }

        return sb.toString();
    }
    public void startSysCall(DockerVMMessage msg) {
        SysCallRecord record = new SysCallRecord(msg.getType());
        sysCalls.add(record);
    }

    public void endSysCall(DockerVMMessage msg) {
        SysCallRecord latestSysCall = getLatestSysCall();
        if (latestSysCall != null) {
            latestSysCall.end();
            addSysCallRecord(latestSysCall);
        }
    }

    public SysCallRecord getLatestSysCall() {
        if (sysCalls.isEmpty()) {
            return null;
        }
        return sysCalls.get(sysCalls.size() - 1);
    }

    public void addSysCallRecord(SysCallRecord record) {
        if (record == null || record.getOpType() == null) {
            return;
        }
        switch (record.getOpType()) {
            case GET_BYTECODE_REQUEST:
            case GET_STATE_REQUEST:
            case GET_BATCH_STATE_REQUEST:
            case CREATE_KV_ITERATOR_REQUEST:
            case CONSUME_KV_ITERATOR_REQUEST:
            case CREATE_KEY_HISTORY_ITER_REQUEST:
            case CONSUME_KEY_HISTORY_ITER_REQUEST:
            case GET_SENDER_ADDRESS_REQUEST:
                sysCallCnt++;
                sysCallDuration += record.getSpentTime();
                break;
            case CALL_CONTRACT_REQUEST:
                crossCallCnt++;
                crossCallDuration += record.getSpentTime();
                break;
            default:
                break;
        }
    }


}


