package org.chainmaker.contracts.docker.java.sandbox.kviterator;

import com.google.protobuf.ByteString;
import org.chainmaker.contracts.docker.java.pb.proto.DockerVMCode;
import org.chainmaker.contracts.docker.java.pb.proto.DockerVMMessage;
import org.chainmaker.contracts.docker.java.pb.proto.DockerVMType;
import org.chainmaker.contracts.docker.java.pb.proto.SysCallMessage;
import org.chainmaker.contracts.docker.java.sandbox.*;
import org.chainmaker.contracts.docker.java.sandbox.utils.ChainUtils;

import java.util.HashMap;
import java.util.Map;

public class KeyHistoryKvIter {
    private final Context context;

    private final RuntimeClient runtimeClient;

    private final String key;

    private final String field;

    private final int index;

    public KeyHistoryKvIter(Context context, RuntimeClient runtimeClient,String key, String field, int index) {
        this.context = context;
        this.runtimeClient = runtimeClient;
        this.key = key;
        this.field = field;
        this.index = index;
    }

    public boolean hasNext() throws InterruptedException, ContractException {
        Map<String, ByteString> params = new HashMap<>(){{
            put(ChainUtils.KeyIteratorFuncName, ByteString.copyFromUtf8(ChainUtils.FuncKeyHistoryIterHasNext));
            put(ChainUtils.KeyIterIndex, ByteString.copyFrom(ChainUtils.leIntToByteArray(index)));
        }};

        DockerVMMessage consumeKvIteratorReq = DockerVMMessage.newBuilder()
                .setChainId(context.getChainId())
                .setTxId(context.getTxId())
                .setType(DockerVMType.CONSUME_KEY_HISTORY_ITER_REQUEST)
                .setCrossContext(context.getCrossCtx())
                .setSysCallMessage(SysCallMessage.newBuilder().putAllPayload(params).build())
                .build();

        DockerVMMessage result = runtimeClient.unarySendMsg(consumeKvIteratorReq);

        if (result.getSysCallMessage().getCode() == DockerVMCode.FAIL){
            throw new ContractException("get history has_next failed, msg: " + result.getSysCallMessage().getMessage());
        }

        ByteString payload = result.getSysCallMessage().getPayloadOrThrow(ChainUtils.KeyIteratorHasNext);

        int has = ChainUtils.byteArrayToLeInt(payload.toByteArray());

        return has != 0;
    }

    public KeyModification next() throws InterruptedException, ContractException {
        Map<String, ByteString> params = new HashMap<>(){{
            put(ChainUtils.KeyIteratorFuncName, ByteString.copyFromUtf8(ChainUtils.FuncKeyHistoryIterNext));
            put(ChainUtils.KeyIterIndex, ByteString.copyFrom(ChainUtils.leIntToByteArray(index)));
        }};

        DockerVMMessage consumeKvIteratorReq = DockerVMMessage.newBuilder()
                .setChainId(context.getChainId())
                .setTxId(context.getTxId())
                .setType(DockerVMType.CONSUME_KEY_HISTORY_ITER_REQUEST)
                .setCrossContext(context.getCrossCtx())
                .setSysCallMessage(SysCallMessage.newBuilder().putAllPayload(params).build())
                .build();

        DockerVMMessage result = runtimeClient.unarySendMsg(consumeKvIteratorReq);

        if (result.getSysCallMessage().getCode() == DockerVMCode.FAIL){
            throw new ContractException("get history next row failed, msg: " + result.getSysCallMessage().getMessage());
        }

        String txId = ChainUtils.byteStringToUTF8String(result.getSysCallMessage().getPayloadOrThrow(ChainUtils.KeyTxId));
        byte[] blockHeightBytes = result.getSysCallMessage().getPayloadOrThrow(ChainUtils.KeyBlockHeight).toByteArray();
        byte[] value = result.getSysCallMessage().getPayloadOrThrow(ChainUtils.KeyStateValue).toByteArray();
        byte[] isDeleteBytes = result.getSysCallMessage().getPayloadOrThrow(ChainUtils.KeyIsDelete).toByteArray();
        String timestamp = ChainUtils.byteStringToUTF8String(result.getSysCallMessage().getPayloadOrThrow(ChainUtils.KeyTimestamp));

        int blockHeight = ChainUtils.byteArrayToLeInt(blockHeightBytes);
        boolean idDelete = ChainUtils.byteArrayToLeInt(isDeleteBytes) == 1;

        return new KeyModification(key, field, value, txId, blockHeight, idDelete, timestamp);
    }


    public void close() throws InterruptedException, ContractException {
        Map<String, ByteString> params = new HashMap<>(){{
            put(ChainUtils.KeyIteratorFuncName, ByteString.copyFromUtf8(ChainUtils.FuncKeyHistoryIterClose));
            put(ChainUtils.KeyIterIndex, ByteString.copyFrom(ChainUtils.leIntToByteArray(index)));
        }};

        DockerVMMessage consumeHistoryKvIteratorReq = DockerVMMessage.newBuilder()
                .setChainId(context.getChainId())
                .setTxId(context.getTxId())
                .setType(DockerVMType.CONSUME_KEY_HISTORY_ITER_REQUEST)
                .setCrossContext(context.getCrossCtx())
                .setSysCallMessage(SysCallMessage.newBuilder().putAllPayload(params).build())
                .build();

        DockerVMMessage result = runtimeClient.unarySendMsg(consumeHistoryKvIteratorReq);

        if (result.getSysCallMessage().getCode() == DockerVMCode.FAIL){
            throw new ContractException("close history iterator failed, msg: " + result.getSysCallMessage().getMessage());
        }
    }
}

