package org.chainmaker.contracts.docker.java.sandbox;

import com.alibaba.fastjson.JSON;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import org.chainmaker.contracts.docker.java.pb.proto.*;
import org.chainmaker.contracts.docker.java.sandbox.kviterator.KeyHistoryKvIter;
import org.chainmaker.contracts.docker.java.sandbox.kviterator.ResultSetKV;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;

import org.chainmaker.contracts.docker.java.sandbox.utils.ChainUtils;
import org.slf4j.Logger;

public class SDK {

    private static RuntimeClient runtimeClient;

    private static Context context;

    private static Logger logger;

    public static String getTxId() {
        return context.getOriginalTxId();
    }

    public static void setRuntimeClient(RuntimeClient runtimeClient){
        SDK.runtimeClient = runtimeClient;
    }

    public static void setContext(Context _context){
        context = _context;
    }

    public static void setLogger(Logger logger){
        SDK.logger = logger;
    }

    private static ByteString getStateFromChain(String key, String field) throws InterruptedException, ContractException, IOException {
        ByteArrayOutputStream concatKey = new ByteArrayOutputStream( );
        concatKey.write( key.getBytes() );
        if (field.length() > 0) {
            concatKey.write( new byte[]{'#'} );
            concatKey.write( field.getBytes() );
        }

        // 拼接 信息
        DockerVMMessage requestMsg = DockerVMMessage.newBuilder()
                .setChainId(context.getChainId())
                .setTxId(context.getTxId())
                .setType(DockerVMType.GET_STATE_REQUEST)
                .setSysCallMessage(
                        SysCallMessage.newBuilder()
                                .putPayload("KEY_CONTRACT_NAME", ByteString.copyFromUtf8(context.getContractName()))
                                .putPayload("KEY_STATE_KEY", ByteString.copyFrom(concatKey.toByteArray()))
                                .build()
                ).build();

        DockerVMMessage responseMsg = runtimeClient.unarySendMsg(requestMsg);


        if (responseMsg.getSysCallMessage().getCode() == DockerVMCode.FAIL) {
            throw new ContractException("get state failed, msg: " + responseMsg.getSysCallMessage().getMessage());
        }

        ByteString value = responseMsg.getSysCallMessage().getPayloadMap().get("KEY_STATE_VALUE");

        return value == null ? ByteString.EMPTY : value;

    }

    // getState 返回 key field 对应的值，如果值不存在则返回空字节数组
    public static byte[] getState(String key, String field) throws InterruptedException, IOException, ContractException {

        ChainUtils.checkKeyFieldStr(key, field);

        String stateKey = context.concatKey(key, field);

        ByteString valueFromWriteSet = context.getWriteSet().get(stateKey);
        if ( valueFromWriteSet != null) {
            context.addReadSet(stateKey, valueFromWriteSet);
            return valueFromWriteSet.toByteArray();
        }

        ByteString valueFromReadSet = context.getReadSet().get(stateKey);
        if ( valueFromReadSet != null) {
            return valueFromReadSet.toByteArray();
        }

        ByteString valueFromChain = getStateFromChain(key, field);
        context.addReadSet(stateKey, valueFromChain);

        return valueFromChain.toByteArray();
    }

    public static byte[] getState(String key) throws IOException, ContractException, InterruptedException {
        return getState(key, "");
    }

    private static List<BatchKey> getBatchStateFromChain(List<BatchKey> batchKeys) throws InterruptedException, IOException, ContractException {

        BatchKeys getBatchStateKeys = BatchKeys.newBuilder().addAllKeys(batchKeys).build();
        byte[] getBatchStateKeysByte = getBatchStateKeys.toByteArray();

        SysCallMessage batchKeySysCallMessage = SysCallMessage.newBuilder()
                .putPayload(ChainUtils.KeyContractName, ByteString.copyFromUtf8(context.getContractName()))
                .putPayload(ChainUtils.KeyStateKey, ByteString.copyFrom(getBatchStateKeysByte))
                .build();

        DockerVMMessage requestMsg = DockerVMMessage.newBuilder()
                .setChainId(context.getChainId())
                .setTxId(context.getTxId())
                .setType(DockerVMType.GET_BATCH_STATE_REQUEST)
                .setCrossContext(context.getCrossCtx())
                .setSysCallMessage(batchKeySysCallMessage)
                .build();

        DockerVMMessage responseMsg = runtimeClient.unarySendMsg(requestMsg);


        if (responseMsg.getSysCallMessage().getCode() == DockerVMCode.FAIL) {
            throw new ContractException("get batch state failed, msg: " + responseMsg.getSysCallMessage().getMessage());
        }

        ByteString value = responseMsg.getSysCallMessage().getPayloadMap().get("KEY_STATE_VALUE");

        List<BatchKey> kvs = BatchKeys.parseFrom(value).getKeysList();

        for ( BatchKey kv : kvs ) {
            // 拼接 keys
            String stateKey = context.concatKey(kv.getKey(), kv.getField());

            context.addReadSet(stateKey, kv.getValue());
        }

        return kvs;

    }

    // putState 写入key field 对应的值，如果写入空字节数组等同于删除这个值
    public static void putState(String key, String field, byte[] value) throws IOException, ContractException {

        ChainUtils.checkKeyFieldStr(key, field);

        String stateKey = context.concatKey(key, field);

        context.addWriteSet(stateKey, ByteString.copyFrom(value));
    }

    public static void putState(String key, String field, String value) throws IOException, ContractException {
        byte[] byteValue = value.getBytes();
        putState(key, field, byteValue);
    }

    public static void putState(String key, String value) throws IOException, ContractException {
        byte[] byteValue = value.getBytes();
        putState(key, "", byteValue);
    }

    public static void putState(String key, byte[] value) throws IOException, ContractException {
        putState(key, "", value);
    }

    // delState 删除某个值，等同于调用putState 写入空字节数组
    public static void delState(String key, String field) throws ContractException, IOException {
        // 写入空字节数组 等同于删除
        putState(key, field, new byte[0]);
    }

    public static void delState(String key) throws ContractException, IOException {
        putState(key, "", new byte[0]);
    }

    public static Response callContract(String contractName, String method, Map<String, ByteString> args)
            throws InterruptedException, InvalidProtocolBufferException {
        // get contract result from runtime server
        // ContractParamSenderAddress is set at the chain
        Map<String, ByteString> initialArgs = new HashMap<>(){{
            put(ChainUtils.ContractCreatorOrgIdParam, ByteString.copyFromUtf8(context.getCreatorOrgId()));
            put(ChainUtils.ContractCreatorRoleParam, ByteString.copyFromUtf8(context.getCreatorRole()));
            put(ChainUtils.ContractCreatorPkParam, ByteString.copyFromUtf8(context.getCreatorPk()));
            put(ChainUtils.ContractSenderOrgIdParam, ByteString.copyFromUtf8(context.getSenderOrgId()));
            put(ChainUtils.ContractSenderRoleParam, ByteString.copyFromUtf8(context.getSenderRole()));
            put(ChainUtils.ContractSenderPkParam, ByteString.copyFromUtf8(context.getSenderPk()));
            put(ChainUtils.ContractBlockHeightParam, ByteString.copyFromUtf8(context.getBlockHeight()));
            put(ChainUtils.ContractTxTimestamp, ByteString.copyFromUtf8(context.getTxTimestamp()));
        }};

        initialArgs.putAll(args);

        CallContractRequest callContractPayloadStruct = CallContractRequest.newBuilder()
                .setContractName(contractName)
                .setContractMethod(method)
                .putAllArgs(initialArgs)
                .build();

        ByteString callContractPayload = callContractPayloadStruct.toByteString();

        SysCallMessage crossSyscallMsg = SysCallMessage.newBuilder()
                .putPayload(ChainUtils.KeyCallContractReq, callContractPayload)
                .build();

        DockerVMMessage callContractReq = DockerVMMessage.newBuilder()
                .setChainId(context.getChainId())
                .setTxId(context.getTxId())
                .setType(DockerVMType.CALL_CONTRACT_REQUEST)
                .setCrossContext(context.getCrossCtx())
                .setSysCallMessage(crossSyscallMsg)
                .build();


        DockerVMMessage callContractResponse = runtimeClient.unarySendMsg(callContractReq);

        // if run into error
        if (callContractResponse.getSysCallMessage().getCode() == DockerVMCode.FAIL) {
            // 跨合约交易报错时，读写集在链端已有保存，因此不需要再合并读写集
            return Response.newBuilder().setStatus(1)
                    .setMessage(callContractResponse.getSysCallMessage().getMessage())
                    .build();
        }

        ByteString callContractResponsePayload = callContractResponse.getSysCallMessage().getPayloadOrThrow(ChainUtils.KeyCallContractResp);

        ContractResponse contractResponse = ContractResponse.parseFrom(callContractResponsePayload);

        // 交易成功需要合并读写集，防止在A跨合约调用B，B再跨合约调用A时，修改了A合约的状态，此时不需要合并事件。
        // merge rwset
        context.getReadSet().putAll(contractResponse.getReadMapMap());

        context.getWriteSet().putAll(contractResponse.getWriteMapMap());

        return contractResponse.getResponse();

    }

    // getParamBytes 获取参数的值，返回字节数组形式
    // 如果没有该参数，返回null
    public static byte[] getParamBytes( String key){
        return context.getParamBytes(key);
    }

    // getParam 获取参数的值，返回字符串形式
    // 如果没有该参数，返回null
    public static String getParam( String key){
        return context.getParamString(key);
    }

    public static Map<String, ByteString> getArgs(){
        return context.getArgs();
    }

    public static void emitEvent(String topic, String[] data){

        Event e = Event.newBuilder().setTopic(topic).setContractName(context.getContractName()).addAllData(List.of(data)).build();

        context.addEvent(e);
    }

    private static ResultSetKV newIterator(String iteratorFuncName, String startKey, String startField, String limitKey, String limitField) throws ContractException, InterruptedException {
        ChainUtils.checkKeyFieldStr(startKey, startField);

        if (Objects.equals(iteratorFuncName, ChainUtils.FuncKvIteratorCreate)) {
            ChainUtils.checkKeyFieldStr(limitKey, limitField);
        }

        Map<String, ByteString> writeSet = context.getWriteSet();

        // 写集需要随syscall请求传回链端，以便构建准确的迭代器，这里清掉写集，不会有问题，还可以减少结果传输时的写集大小
        Map<String, ByteString> createKvIteratorParams = new HashMap<>(){{
            put(ChainUtils.KeyContractName, ByteString.copyFromUtf8(context.getContractName()));
            put(ChainUtils.KeyIteratorFuncName, ByteString.copyFromUtf8(iteratorFuncName));
            put(ChainUtils.KeyIterStartKey, ByteString.copyFromUtf8(startKey));
            put(ChainUtils.KeyIterStartField, ByteString.copyFromUtf8(startField));
            put(ChainUtils.KeyIterLimitKey, ByteString.copyFromUtf8(limitKey));
            put(ChainUtils.KeyIterLimitField, ByteString.copyFromUtf8(limitField));
            put(ChainUtils.KeyWriteMap, ByteString.copyFrom(JSON.toJSONBytes(writeSet)));
        }};

        // todo 测试一下 清空写集 导致的读错误问题
        // 清掉写集，会导致特殊情况下的读错误：合约已有一些读写集，清空写集后，后续的读操作只能从缓存的读集中读取，导致读数据不一致
        // reset writeset
        // context.resetWrxiteSet();

        SysCallMessage createKvIteratorSyscallMsg = SysCallMessage.newBuilder()
                .putAllPayload(createKvIteratorParams)
                .build();

        DockerVMMessage createKvIteratorReq = DockerVMMessage.newBuilder()
                .setChainId(context.getChainId())
                .setTxId(context.getTxId())
                .setType(DockerVMType.CREATE_KV_ITERATOR_REQUEST)
                .setCrossContext(context.getCrossCtx())
                .setSysCallMessage(createKvIteratorSyscallMsg)
                .build();

        DockerVMMessage createKvIteratorResponse = runtimeClient.unarySendMsg(createKvIteratorReq);

        // if run into error
        if (createKvIteratorResponse.getSysCallMessage().getCode() == DockerVMCode.FAIL) {
            throw new ContractException("create iterator failed, msg: " + createKvIteratorResponse.getSysCallMessage().getMessage());
        }

        ByteString value = createKvIteratorResponse.getSysCallMessage().getPayloadOrThrow(ChainUtils.KeyIterIndex);

        int index = ChainUtils.byteArrayToLeInt(value.toByteArray());

        return new ResultSetKV(context, runtimeClient, index);
    }

    public static ResultSetKV newIterator(String startKey, String limitKey) throws ContractException, InterruptedException {
        return newIterator(ChainUtils.FuncKvIteratorCreate, startKey, "", limitKey, "");
    }

    public static ResultSetKV newIteratorWithField(String key, String startField, String limitField) throws ContractException, InterruptedException {
        return newIterator(ChainUtils.FuncKvIteratorCreate, key, startField, key, limitField);
    }

    public static ResultSetKV newIteratorPrefixWithKeyField(String startKey, String startField) throws ContractException, InterruptedException {
        return newIterator(ChainUtils.FuncKvPreIteratorCreate, startKey, startField, "", "");
    }

    public static ResultSetKV newIteratorPrefixWithKey(String key) throws ContractException, InterruptedException {
        return newIterator(ChainUtils.FuncKvPreIteratorCreate, key, "", "", "");
    }

    public static KeyHistoryKvIter newHistoryKvIterForKey(String key, String field) throws ContractException, InterruptedException {
        ChainUtils.checkKeyFieldStr(key, field);

        Map<String, ByteString> writeSet = context.getWriteSet();

        // 写集需要随syscall请求传回链端，以便构建准确的迭代器，这里清掉写集，不会有问题，还可以减少结果传输时的写集大小
        Map<String, ByteString> createHistoryKVIterParams = new HashMap<>(){{
            put(ChainUtils.KeyContractName, ByteString.copyFromUtf8(context.getContractName()));
            put(ChainUtils.KeyHistoryIterKey, ByteString.copyFromUtf8(key));
            put(ChainUtils.KeyHistoryIterField, ByteString.copyFromUtf8(field));
            put(ChainUtils.KeyWriteMap, ByteString.copyFrom(JSON.toJSONBytes(writeSet)));
        }};

        // 清掉写集，会导致特殊情况下的读错误：合约已有一些读写集，清空写集后，后续的读操作只能从缓存的读集中读取，导致读数据不一致

        SysCallMessage createHistoryKvIteratorSyscallMsg = SysCallMessage.newBuilder()
                .putAllPayload(createHistoryKVIterParams)
                .build();

        DockerVMMessage createHistoryKvIteratorReq = DockerVMMessage.newBuilder()
                .setChainId(context.getChainId())
                .setTxId(context.getTxId())
                .setType(DockerVMType.CREATE_KEY_HISTORY_ITER_REQUEST)
                .setCrossContext(context.getCrossCtx())
                .setSysCallMessage(createHistoryKvIteratorSyscallMsg)
                .build();

        DockerVMMessage createHistoryKvIteratorResponse = runtimeClient.unarySendMsg(createHistoryKvIteratorReq);

        // if run into error
        if (createHistoryKvIteratorResponse.getSysCallMessage().getCode() == DockerVMCode.FAIL) {
            throw new ContractException("create history iterator failed, msg: " + createHistoryKvIteratorResponse.getSysCallMessage().getMessage());
        }

        ByteString value = createHistoryKvIteratorResponse.getSysCallMessage().getPayloadOrThrow(ChainUtils.KeyIterIndex);

        int index = ChainUtils.byteArrayToLeInt(value.toByteArray());

        return new KeyHistoryKvIter(context, runtimeClient, key, field, index);
    }

    public static List<BatchKey> getBatchState(List<BatchKey> keys) throws IOException, ContractException, InterruptedException {
        // 检查长度
        if (keys.size() > 10000) {
            throw new ContractException("over batch keys count limit 10000");
        }
        for ( BatchKey key: keys ) {
            ChainUtils.checkKeyFieldStr(key.getKey(), key.getField());
        }

        List<BatchKey> gotKVs = new ArrayList<>();
        List<BatchKey> KVsFromChain = new ArrayList<>();

    //    检查
        for( BatchKey key: keys){
            BatchKey gotKV = getStateFromRWSet(key);
            if (gotKV == null) {
                KVsFromChain.add(key.toBuilder().setContractName(context.getContractName()).build());
            } else {
                gotKVs.add(gotKV);
            }
        }

        List<BatchKey> gotKVsFromChain = getBatchStateFromChain(KVsFromChain);

        gotKVs.addAll(gotKVsFromChain);

        return gotKVs;

    }

    private static BatchKey getStateFromRWSet(BatchKey key) throws IOException {

        ByteString writeSetValue;
        ByteString readSetValue;

        // 拼接 keys
        String stateKey = context.concatKey(key.getKey(), key.getField());

        writeSetValue = context.getWriteSet().get(stateKey);

        if ( writeSetValue != null) {
            context.addReadSet(stateKey, writeSetValue);
            return key.toBuilder().setValue(writeSetValue).build();
        }

        readSetValue = context.getReadSet().get(stateKey);

        if ( readSetValue != null) {
            return key.toBuilder().setValue(readSetValue).build();
        }

        return null;

    }


    public static String origin() throws InterruptedException, ContractException {
        if (context.getOrigin() != null && context.getOrigin().length() > 0) {
            return context.getOrigin();
        }

        DockerVMMessage getSenderAddrReq = DockerVMMessage.newBuilder()
                .setChainId(context.getChainId())
                .setTxId(context.getTxId())
                .setType(DockerVMType.GET_SENDER_ADDRESS_REQUEST)
                .setCrossContext(context.getCrossCtx())
                .build();

        DockerVMMessage getSenderAddrResponse = runtimeClient.unarySendMsg(getSenderAddrReq);

        if (getSenderAddrResponse.getSysCallMessage().getCode() == DockerVMCode.FAIL) {
            throw new ContractException("get origin failed, msg: " + getSenderAddrResponse.getSysCallMessage().getMessage());
        }

        ByteString value = getSenderAddrResponse.getSysCallMessage().getPayloadOrThrow(ChainUtils.KeySenderAddr);

        context.setOrigin(value.toStringUtf8());

        return value.toStringUtf8();

    }

    public static String sender() throws InterruptedException, ContractException {

        if (context.getCrossCtx().getCurrentDepth() > 0){
            if (context.getSenderAddress().length() == 0) {
                throw new ContractException("can not get sender");
            } else {
                return context.getSenderAddress();
            }
        }

        return origin();

    }

    public static void logW(String msg){
        logger.warn(msg);
    }

    public static void logI(String msg){
        logger.info(msg);
    }

    public static void logD(String msg){
        logger.debug(msg);
    }

    public static void logE(String msg){
        logger.error(msg);
    }


    // from context
    public static String getCreatorOrgId(){
        return context.getCreatorOrgId();
    }
    public static String getCreatorRole(){
        return context.getCreatorRole();
    }
    public static String getCreatorPk(){
        return context.getCreatorPk();
    }
    public static String getSenderOrgId(){
        return context.getSenderOrgId();
    }
    public static String getSenderRole(){
        return context.getSenderRole();
    }
    public static String getSenderPk(){
        return context.getSenderPk();
    }


    public static int getBlockHeight(){
        return Integer.parseInt(context.getBlockHeight());
    }

    public static Response getTxInfo(String txId) throws InvalidProtocolBufferException, InterruptedException {
        String paramTxId = "txId";
        String paramMethod = "method";

        String contractName = "CHAIN_QUERY";
        String method ="GET_TX_BY_TX_ID";

        Map<String, ByteString> args = new HashMap<>(){{
            put(paramTxId, ByteString.copyFromUtf8(txId));
            put(paramMethod, ByteString.copyFromUtf8(method));
        }};

        return SDK.callContract(contractName, method, args);
    }

    public static String getTxTimestamp(){
        return context.getTxTimestamp();
    }


    public static Response success(String resultMsg) {
        return success(resultMsg.getBytes());
    }

    public static Response success(byte[] resultMsg) {
        return Response.newBuilder()
                .setStatus(DockerVMCode.OK_VALUE)
                .setPayload(ByteString.copyFrom(resultMsg))
                .build();
    }

    public static Response error(String resultMsg) {
        return Response.newBuilder()
                .setStatus(DockerVMCode.FAIL_VALUE)
                .setMessage(resultMsg)
                .build();
    }

}

