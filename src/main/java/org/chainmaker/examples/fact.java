package org.chainmaker.examples;

import com.google.protobuf.ByteString;
import org.chainmaker.contracts.docker.java.pb.proto.Response;
import org.chainmaker.contracts.docker.java.sandbox.IContract;
import org.chainmaker.contracts.docker.java.sandbox.ContractException;
import org.chainmaker.contracts.docker.java.sandbox.SDK;
import org.chainmaker.contracts.docker.java.sandbox.Sandbox;
import org.chainmaker.contracts.docker.java.sandbox.annotaion.ContractMethod;

import java.io.IOException;


public class fact implements IContract {

    @ContractMethod
    public Response save() throws IOException, ContractException {
        String key = SDK.getParam("key");
        byte[] value = SDK.getParamBytes("value");

        SDK.putState(key, value);

        return SDK.success("store success");
    }

    @ContractMethod
    public Response get() throws IOException, InterruptedException, ContractException {
        String key = SDK.getParam("key");
        return SDK.success(SDK.getState(key));
    }

    public static void main(String[] args) {
        Sandbox.serve(args, new fact());
    }

}
